import React from "react";
import { GameContainer } from "./component/GameZone";
import Game from "./page/Game";
import Interface from "./component/Interface";

import "./App.css";

window.addEventListener("keydown", e => {
  if (e.code === "Space") {
    alert("Pause");
  }
});

function App() {
  return (
    <GameContainer>
      <Interface></Interface>
      <Game></Game>
    </GameContainer>
  );
}

export default App;
