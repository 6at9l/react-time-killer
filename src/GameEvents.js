export const BATTLE_AREA_CLICK_EVENT = data => {
  const _event = new CustomEvent("Event", { detail: data });
  _event.initEvent("BATTLE_AREA_CLICK_EVENT", true, true);
  return _event;
};

export const WIZARD_SHOT = data => {
  const _event = new CustomEvent("Event", { detail: data });
  _event.initEvent("WIZARD_SHOT", true, true);
  return _event;
};

export const MONSTER_TIME_LIFE = index => {
  const _event = new CustomEvent("Event", { detail: index });
  _event.initEvent("MONSTER_TIME_LIFE", true, true);
  return _event;
};
