import React, { Component } from "react";
import styled from "styled-components";

const Header = styled.div`
  height: 50px;
  width: 100%;
  display: flex;
  background: gray;

  color: white;
  font-size: 40px;
  font-family: fantasy;
  justify-content: center;
  align-items: center;
`;

export default class Interface extends Component {
  state = {
    score: 0
  };

  onCount = e => {
    this.setState(({ score }) => {
      return {
        score: score + e.detail
      };
    });
  };

  componentDidMount() {
    window.addEventListener("MONSTER_TIME_LIFE", this.onCount);
  }

  componentWillUnmount() {
    window.removeEventListener("MONSTER_TIME_LIFE", this.onCount);
  }

  render() {
    return <Header>{this.state.score}</Header>;
  }
}
