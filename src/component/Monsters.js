import React, { Component, Fragment, useEffect } from "react";
import styled from "styled-components";
import bgMonster from "../assets/monster.png";
import { MONSTER_TIME_LIFE } from "../GameEvents";

const Monster = styled.div.attrs(({ dataKey }) => {
  return {
    "data-type": "MONSTER",
    "data-key": dataKey
  };
})`
  background-image: url(${bgMonster});
  width: 48px;
  height: 48px;
  top: ${({ top = 0 }) => `${top}px`};
  left: ${({ left = 0 }) => `${left}px`};
  background-position: 0px -228px;
  background-repeat: no-repeat;
  position: absolute;
  z-index: 2;
  transition-property: opacity top left;
  transition-duration: 1s;
  transition-timing-function: linear;

  animation: move 0.5s infinite step-start;

  @keyframes move {
    0% {
      background-position: 0px -228px;
    }
    25% {
      background-position: -48px -228px;
    }
    50% {
      background-position: -96px -228px;
    }
    75% {
      background-position: -48px -228px;
    }
    100% {
      background-position: 0px -228px;
    }
  }
`;

export class MonstersArea extends Component {
  monsterMoveTimer = null;
  limit = 4;
  slots = [80, 160, 320, 400, 480, 560, 640, 720, 800, 880];
  slotDisable = [];

  state = {
    monsters: []
  };

  onShot = async ({ detail: { type, key } }) => {
    if (type === "MONSTER") {
      await new Promise(res => setTimeout(res, 300));
      window.dispatchEvent(MONSTER_TIME_LIFE(1));
      this.setState({
        monsters: this.state.monsters.filter(e => e.key !== key)
      });
    }
  };

  monsterFactory = async () => {
    const [position] = this.slots.splice(
      Math.random(Math.random() * (this.slots.length - 1)),
      1
    );

    if (position) {
      const monster = {
        top: 590,
        left: position,
        key: Math.random()
          .toString(36)
          .replace("0.", "key")
      };

      await new Promise(res => {
        this.setState({ monsters: [...this.state.monsters, monster] }, () => {
          setTimeout(res, 2000);
        });
      });
      this.slots.push(position);
    }
  };

  monsterMove = () => {
    const monsters = this.state.monsters
      .filter(e => e.top > 40)
      .map(e => {
        return { ...e, top: e.top - (Math.random() * 40 + 40) };
      });

    window.dispatchEvent(
      MONSTER_TIME_LIFE(monsters.length - this.state.monsters.length)
    );
    this.setState({
      monsters
    });
  };

  componentDidMount() {
    this.monsterMoveTimer = setInterval(() => {
      if (this.state.monsters.length < this.limit && Math.random() > 0) {
        this.monsterFactory();
      }
      this.monsterMove();
    }, 1000);
    window.addEventListener("WIZARD_SHOT", this.onShot);
  }

  componentWillUnmount() {
    clearInterval(this.monsterMoveTimer);
    window.removeEventListener("WIZARD_SHOT", this.onShot);
  }

  render() {
    return (
      <Fragment>
        {this.state.monsters.map(e => (
          <Monster key={e.key} dataKey={e.key} top={e.top} left={e.left} />
        ))}
      </Fragment>
    );
  }
}
