import styled from "styled-components";
import bg from "../assets/background.png";

export const BattleArea = styled.div`
  width: 960px;
  height: 640px;
  position: relative;
  background: #805f31;

  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
  align-items: flex-start;
  border: 20px solid #b17f37;
`;

export const BattleBlock = styled.div.attrs(() => {
  return {
    "data-type": "BLOCK"
  };
})`
  background-image: url(${bg});
  background-position: 535px -75px;
  width: 80px;
  height: 80px;
`;

export const GameContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  height: 100vh;
  width: 100vw;
  overflow: auto;
`;
