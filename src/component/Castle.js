import React, { Component, Fragment } from "react";
import styled from "styled-components";
import bg from "../assets/background.png";
import wizard from "../assets/wizard.png";
import { WIZARD_SHOT } from "../GameEvents";

const Wall = styled.div`
  background-image: url(${bg});
  background-position: 280px -885px;
  background-size: 1280px 1085px;
  width: 80px;
  height: 80px;
  top: ${({ top = 0 }) => `${top}px`};
  left: ${({ left = 0 }) => `${left}px`};
  position: absolute;
  z-index: 1;
`;

const t = 0.5;
const WizardWell = styled.div`
  background-image: url(${bg});
  background-position: 280px -885px;
  background-size: 1280px 1085px;
  width: 80px;
  height: 80px;
  top: ${({ top = 0 }) => `${top}px`};
  left: ${({ left = 0 }) => `${left}px`};
  position: absolute;
  z-index: 1;

  &::after {
    content: "";
    display: block;
    position: absolute;
    background-image: url(${wizard});
    ${({ isActive = false }) => {
      if (isActive) {
        return "background-position: -500px -1145px;";
      } else {
        return "background-position: -125px -1017px;";
      }
    }}
    background-repeat: none;
    background-size: 662.5px 678px;
    width: 80px;
    height: 60px;
    top: 10px;
  }

  &::before {
    content: "${({ energy }) => energy}";
    color: white;
    display: block;
    position: absolute;
    width: 100%;
    background: linear-gradient(to right, rgba(0, 0, 255) 0%, rgba(0, 0, 255, 0.3) ${({
      energy
    }) => energy}%);
    text-align: center;
    bottom: 0;
  }
`;

export class Castle extends Component {
  manaRegenerationTimer = null;

  constructor(props) {
    super(props);

    const wells = [];

    for (let i = 0; i < props.column; i++) {
      wells.push({
        type: i == 0 || i == props.column - 1 ? "WIZARD_WELL" : "WELL",
        energy: 100
      });
    }

    this.state = { wells, activeWizardIndex: null };
  }

  pickWizard = activeWizardIndex => () => this.setState({ activeWizardIndex });

  onTryShot = e => {
    if (
      this.state.activeWizardIndex !== null &&
      this.state.activeWizardIndex >= 0 &&
      this.state.wells[this.state.activeWizardIndex].energy >= 10
    ) {
      this.setState(
        state => {
          return {
            wells: state.wells.map((e, i) => {
              if (i === state.activeWizardIndex) {
                return {
                  ...e,
                  energy: e.energy - 10
                };
              }
              return e;
            })
          };
        },
        () => {
          window.dispatchEvent(WIZARD_SHOT({ ...e.detail }));
        }
      );
    }
  };

  componentDidMount() {
    window.addEventListener("BATTLE_AREA_CLICK_EVENT", this.onTryShot);
    this.manaRegenerationTimer = setInterval(() => {
      this.setState(state => {
        return {
          wells: state.wells.map((e, i) => {
            if (i !== state.activeWizardIndex && e.energy < 100) {
              return {
                ...e,
                energy: e.energy + 10
              };
            }
            return e;
          })
        };
      });
    }, 500);
  }

  componentWillUnmount() {
    window.removeEventListener("BATTLE_AREA_CLICK_EVENT", this.onTryShot);
    clearInterval(this.manaRegenerationTimer);
  }

  render() {
    return (
      <Fragment>
        {this.state.wells.map((e, i) => {
          if (e.type === "WELL") {
            return <Wall left={80 * i} key={e.type + i} />;
          } else if (e.type === "WIZARD_WELL") {
            return (
              <WizardWell
                isActive={i === this.state.activeWizardIndex}
                left={80 * i}
                energy={e.energy}
                key={e.type + i}
                onClick={this.pickWizard(i)}
              />
            );
          }
        })}
      </Fragment>
    );
  }
}
