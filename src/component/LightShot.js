import React, { useState } from "react";
import styled from "styled-components";
import bgLight from "../assets/lightShot.png";

const Shot = styled.div`
  background-image: url(${bgLight});
  background-size: cover;
  width: 80px;
  height: 80px;
  top: ${({ top = 0 }) => `${top}px`};
  left: ${({ left = 0 }) => `${left}px`};
  position: absolute;
  z-index: 3;
  opacity: ${({ factor }) => factor};
  transition-duration: 0.5s;
  transition-property: opacity scale;
  transform: translate(-50%, -50%) scale(${({ factor }) => factor});
`;

export const LightShot = ({ top, left, onEnd }) => {
  const [factor, setFactor] = useState(0);

  useState(() => {
    (async function() {
      await new Promise(res => setTimeout(() => res(setFactor(1)), 100));
      await new Promise(res => setTimeout(() => res(setFactor(0)), 500));
      setTimeout(() => onEnd && onEnd(1), 500);
    })();
  }, []);

  return <Shot top={top} left={left} factor={factor} />;
};
