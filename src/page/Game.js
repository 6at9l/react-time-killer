import React, { Component, createRef } from "react";
import { BattleArea, BattleBlock } from "../component/GameZone";
import { Castle } from "../component/Castle";
import { LightShot } from "../component/LightShot";
import { BATTLE_AREA_CLICK_EVENT } from "../GameEvents";
import { MonstersArea } from "../component/Monsters";

export default class Game extends Component {
  column = 12;
  row = 8;

  state = {
    shots: []
  };

  battleRef = createRef();

  blockFactory = () => {
    const _block = [];
    for (let i = this.row * this.column; i--; ) {
      _block.push(<BattleBlock key={`block_${i}`} />);
    }

    return _block;
  };

  removeShot = key => () => {
    this.setState(state => ({
      shots: state.shots.filter(e => e.key !== key)
    }));
  };

  onShot = e => {
    this.setState(state => ({
      shots: [
        ...state.shots,
        {
          top: e.detail.y,
          left: e.detail.x,
          key: Math.random()
            .toString(36)
            .replace("0.", "key")
        }
      ]
    }));
  };

  componentDidMount() {
    window.addEventListener("WIZARD_SHOT", this.onShot);
  }

  componentWillUnmount() {
    window.removeEventListener("WIZARD_SHOT", this.onShot);
  }

  onBattleAreaClick = e => {
    const type = e.target.getAttribute("data-type");
    const key = e.target.getAttribute("data-key");

    if (this.battleRef.current && ["BLOCK", "MONSTER"].some(e => e === type)) {
      const { x, y } = this.battleRef.current.getBoundingClientRect();
      window.dispatchEvent(
        BATTLE_AREA_CLICK_EVENT({
          x: e.clientX - x - 20,
          y: e.clientY - y - 20,
          type: e.target.getAttribute("data-type"),
          key
        })
      );
    }
  };

  render() {
    return (
      <BattleArea ref={this.battleRef} onClick={this.onBattleAreaClick}>
        {this.blockFactory()}
        {this.state.shots.map(e => (
          <LightShot {...e} onEnd={this.removeShot(e.key)} />
        ))}
        <Castle column={this.column} />
        <MonstersArea />
      </BattleArea>
    );
  }
}
